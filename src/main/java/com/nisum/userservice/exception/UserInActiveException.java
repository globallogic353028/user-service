package com.nisum.userservice.exception;

public class UserInActiveException extends RuntimeException {

    public static final String DEFAULT_MSG_USER_INACTIVE = "User inactive contact admin";

    public UserInActiveException(String message) {
        super(message);
    }

    public UserInActiveException(String message, Throwable cause) {
        super(message, cause);
    }
}
