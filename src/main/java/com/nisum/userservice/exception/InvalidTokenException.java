package com.nisum.userservice.exception;

public class InvalidTokenException extends RuntimeException {

    public static final String DEFAULT_MSG_INVALID_TOKEN = "Invalid Token";

    public InvalidTokenException(String message) {
        super(message);
    }

    public InvalidTokenException(String message, Throwable cause) {
        super(message, cause);
    }

}
