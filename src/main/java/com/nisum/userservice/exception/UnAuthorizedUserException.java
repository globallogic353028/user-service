package com.nisum.userservice.exception;

public class UnAuthorizedUserException extends RuntimeException {

    public static final String DEFAULT_MSG_UNAUTHORIZED_USER = "Email user or password invalid";

    public UnAuthorizedUserException(String message) {
        super(message);
    }

    public UnAuthorizedUserException(String message, Throwable cause) {
        super(message, cause);
    }

}
