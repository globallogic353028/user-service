package com.nisum.userservice.exception;

public class UserExistException extends RuntimeException {

    public static final String DEFAULT_MSG_USER_EXIST = "Exist User";


    public UserExistException(String message) {
        super(message);
    }

    public UserExistException(String message, Throwable cause) {
        super(message, cause);
    }

}
