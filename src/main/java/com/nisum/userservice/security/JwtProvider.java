package com.nisum.userservice.security;

import com.nisum.userservice.entity.User;
import com.nisum.userservice.exception.InvalidTokenException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;

@Component
public class JwtProvider {

    @Value("jwt.secret")
    private String secret;

    @PostConstruct
    protected void init(){
        this.secret = Base64.getEncoder().encodeToString(secret.getBytes());
    }

    public String createToken(User user){
        Map<String, Object> claims = new HashMap<>();
        claims = Jwts.claims().setSubject(user.getEmail());
        claims.put("id", user.getId());
        Date create = new Date();
        Date expiration = new Date(create.getTime() + 3600000);
        return  Jwts.builder().setClaims(claims)
                .setIssuedAt(create)
                .setExpiration(expiration)
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }



    public boolean validate(String token) {
        try {
            Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
            return true;
        }catch (Exception e){
            return false;
        }
    }



    public String getUserNameFromToken(String token){
        try {
            return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getSubject();
        }catch (Exception e) {
           throw new InvalidTokenException(InvalidTokenException.DEFAULT_MSG_INVALID_TOKEN);
        }
    }
}
