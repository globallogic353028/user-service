package com.nisum.userservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Set;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDTO {

    private Long id;

    private String name;

    @NotEmpty(message = "The email address is required.")
    @Email(message = "The email address is invalid.", flags = { Pattern.Flag.CASE_INSENSITIVE })
    private String email;

    @NotEmpty(message = "The password  is required.")
    @Size(min = 8, message = "The password must have 8 characters min")
    @Size(max = 12, message = "The password must have 12 characters max")
    @Pattern(regexp = "^(?=(?:\\D*\\d){2}\\D*$)([^A-Z]*[A-Z][^A-Z]*$)" , message="password must have 2 number and 1 upper char")
    private String password;

    private Set<PhoneDTO> phones;

}
