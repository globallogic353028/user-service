package com.nisum.userservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PhoneDTO {

    @NotNull(message = "city is required")
    @Pattern(regexp = "[\\s]*[0-9]*[1-9]+",message="is must be a positive number")
    private Integer cityCode;

    @NotEmpty(message = "countryCode is required.")
    private String countryCode ;

    @NotNull(message = "number is required")
    @Pattern(regexp = "[\\s]*[0-9]*[1-9]+",message="is must be a  positive number")
    private Integer number;

    public Integer getCityCode() {
        return cityCode;
    }

    public void setCityCode(Integer cityCode) {
        this.cityCode = cityCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
}
