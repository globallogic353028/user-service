package com.nisum.userservice.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ErrorResponse {
    private Integer codigo;
    private Timestamp timestamp;
    private String detail;
}
