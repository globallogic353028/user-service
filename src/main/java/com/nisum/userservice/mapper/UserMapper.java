package com.nisum.userservice.mapper;

import com.nisum.userservice.dto.LogUserDTO;
import com.nisum.userservice.dto.SignUpDTO;
import com.nisum.userservice.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel="spring")
public interface UserMapper {
    SignUpDTO userToDto(User user);

    LogUserDTO usertToLogUserDTO(User user);
}




