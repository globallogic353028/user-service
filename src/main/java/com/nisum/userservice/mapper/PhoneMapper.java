package com.nisum.userservice.mapper;

import com.nisum.userservice.dto.PhoneDTO;
import com.nisum.userservice.entity.Phone;
import org.mapstruct.Mapper;

import java.util.Set;

@Mapper(componentModel="spring")
public interface PhoneMapper {

    Set<Phone> listPhoneDTOToListPhone(Set<PhoneDTO> phone);
    Phone phoneDTOTOphone(PhoneDTO phone);
}
