package com.nisum.userservice.handler;

import com.nisum.userservice.exception.InvalidTokenException;
import com.nisum.userservice.exception.UserExistException;
import com.nisum.userservice.exception.UserInActiveException;
import com.nisum.userservice.exception.UnAuthorizedUserException;
import com.nisum.userservice.response.ApiErrorResponse;
import com.nisum.userservice.response.ErrorResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@RestControllerAdvice
public class ExceptionHandler extends ResponseEntityExceptionHandler {

    public static final Integer HTTP_STATUS_INVALID_TOKEN = 498;

    @org.springframework.web.bind.annotation.ExceptionHandler({Throwable.class})
    @ResponseBody
    public Object handleExceptions(HttpServletRequest req, HttpServletResponse resp, Throwable ex) {

        //HAGO ESTO PORQUE EL ENUNCIADO PIDE UN ARRAY EN LA RESPUESTA DEL SERVICIO PERO NO TIENE SENTIDO
        //SIEMPRE EL STATUS ES UNICO LO HAGO PARA CUMPLIR CON LO PEDIDO
        ApiErrorResponse apiErrorResponse  = new ApiErrorResponse();
        List<ErrorResponse> listErrors = new ArrayList<>();
        if (ex instanceof UserExistException) {
            listErrors.add(new ErrorResponse(HttpStatus.CONFLICT.value(), new Timestamp(System.currentTimeMillis()), ex.getMessage()));
        }
        else if(ex instanceof InvalidTokenException) {
            listErrors.add(new ErrorResponse(HTTP_STATUS_INVALID_TOKEN, new Timestamp(System.currentTimeMillis()), ex.getMessage()));
        }
        else if(ex instanceof UserInActiveException) {
            listErrors.add(new ErrorResponse(HttpStatus.FORBIDDEN.value(), new Timestamp(System.currentTimeMillis()), ex.getMessage()));
        }
        else if(ex instanceof UnAuthorizedUserException) {
            listErrors.add(new ErrorResponse(HttpStatus.UNAUTHORIZED.value(), new Timestamp(System.currentTimeMillis()), ex.getMessage()));
        }
        else if (ex instanceof HttpClientErrorException.BadRequest) {
            listErrors.add(new ErrorResponse(HttpStatus.BAD_REQUEST.value(), new Timestamp(System.currentTimeMillis()), ex.getMessage()));
        }else {
            resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            logger.error("Unexpected error has ocurred. Details: \n", ex);
            listErrors.add(new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), new Timestamp(System.currentTimeMillis()), ex.getMessage()));
        }
        apiErrorResponse.setErrors(listErrors);
        return apiErrorResponse;
    }


    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        ApiErrorResponse apiErrorResponse  = new ApiErrorResponse();
        List<ErrorResponse> listErrors = new ArrayList<>();
        Map<String, List<String>> body = new HashMap<>();

        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());
        listErrors.add(new ErrorResponse(HttpStatus.BAD_REQUEST.value(), new Timestamp(System.currentTimeMillis()), errors.toString()));
        apiErrorResponse.setErrors(listErrors);
        return new ResponseEntity<>(apiErrorResponse, HttpStatus.BAD_REQUEST);
    }
    }



