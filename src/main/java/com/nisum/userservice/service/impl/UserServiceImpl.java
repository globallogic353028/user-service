package com.nisum.userservice.service.impl;


import com.nisum.userservice.dto.LogUserDTO;
import com.nisum.userservice.dto.LoginDTO;
import com.nisum.userservice.dto.SignUpDTO;
import com.nisum.userservice.dto.UserDTO;
import com.nisum.userservice.entity.Phone;
import com.nisum.userservice.entity.User;
import com.nisum.userservice.exception.InvalidTokenException;
import com.nisum.userservice.exception.UserExistException;
import com.nisum.userservice.exception.UserInActiveException;
import com.nisum.userservice.exception.UnAuthorizedUserException;
import com.nisum.userservice.mapper.PhoneMapper;
import com.nisum.userservice.mapper.UserMapper;
import com.nisum.userservice.repository.UserRepository;
import com.nisum.userservice.security.JwtProvider;
import com.nisum.userservice.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service("userService")
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtProvider jwtProvider;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PhoneMapper phoneMapper;

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);




    @Override
    public SignUpDTO signUp(UserDTO userDTO) {
        LOGGER.debug("SignUp user {}" , userDTO.getEmail());

        Optional<User> optionalUser = userRepository.findUserByEmail(userDTO.getEmail());
        if(optionalUser.isPresent()){
            throw  new UserExistException(UserExistException.DEFAULT_MSG_USER_EXIST);
        }

        User userSign = User.builder()
                .password(passwordEncoder.encode(userDTO.getPassword()))
                .email(userDTO.getEmail())
                .name(userDTO.getName())
                .created(LocalDateTime.now())
                .isActive(Boolean.TRUE)
                .build();

        //creamos el token
        String firstToken = jwtProvider.createToken(userSign);
        userSign.setToken(firstToken);

        if(!CollectionUtils.isEmpty(userDTO.getPhones())){
            Set<Phone> phoneList = phoneMapper.listPhoneDTOToListPhone(userDTO.getPhones());
            //asociamos
            phoneList.forEach(phone -> phone.setUser(userSign));
            userSign.setPhones(phoneList);
        }

        return userMapper.userToDto(userRepository.save(userSign));
    }

    @Override
    public List<SignUpDTO> getUsers() {
        LOGGER.debug("Find All User ...");
        //si bien lo puedo hacer con el mapper lo hago asi para usar streams de java8 segun lo pedido
        return this.userRepository.findAll().stream().map(user -> userMapper.userToDto(user)).collect(Collectors.toList());

    }

    @Override
    public LogUserDTO login(LoginDTO loginDTO) {
        LOGGER.debug("Login user {}" , loginDTO.getEmail());
        User user = userRepository.findUserByEmail(loginDTO.getEmail()).orElseThrow(() -> new UnAuthorizedUserException(UnAuthorizedUserException.DEFAULT_MSG_UNAUTHORIZED_USER));
        if(!passwordEncoder.matches(loginDTO.getPassword(), user.getPassword()))
           throw  new UnAuthorizedUserException(UnAuthorizedUserException.DEFAULT_MSG_UNAUTHORIZED_USER);

        if (!jwtProvider.validate(loginDTO.getToken()))
            throw  new InvalidTokenException(InvalidTokenException.DEFAULT_MSG_INVALID_TOKEN);

        if(!user.getActive())
            throw  new UserInActiveException(UserInActiveException.DEFAULT_MSG_USER_INACTIVE);
        //actualizamos el last login
        user.setLastLogin(LocalDateTime.now());
        //creamos un nuevo token valido
        String newToken = jwtProvider.createToken(user);
        user.setToken(newToken);
        //actualizamos el usuario
        userRepository.save(user);

        return  userMapper.usertToLogUserDTO(user);

    }
}
