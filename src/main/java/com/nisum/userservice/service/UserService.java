package com.nisum.userservice.service;

import com.nisum.userservice.dto.LogUserDTO;
import com.nisum.userservice.dto.LoginDTO;
import com.nisum.userservice.dto.SignUpDTO;
import com.nisum.userservice.dto.UserDTO;

import java.util.List;

public interface UserService {
    SignUpDTO signUp(UserDTO userDTO);

    List<SignUpDTO> getUsers();

    LogUserDTO login(LoginDTO userDTO);
}
