package com.nisum.userservice.controller;

import com.nisum.userservice.dto.LogUserDTO;
import com.nisum.userservice.dto.LoginDTO;
import com.nisum.userservice.dto.SignUpDTO;
import com.nisum.userservice.dto.UserDTO;
import com.nisum.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;


@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PATCH, RequestMethod.PUT, RequestMethod.DELETE})
public class UserControllerImpl implements UserController{

    @Autowired
    private UserService userService;

    @Override
    @PostMapping("/sign-up")
    public ResponseEntity<SignUpDTO>  signUp(@Valid @RequestBody UserDTO userDTO){
        SignUpDTO dto = userService.signUp(userDTO);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(dto.getId()).toUri();
         return ResponseEntity.created(location).body(dto);
    }

    @GetMapping
    public ResponseEntity<List<SignUpDTO>> getUsers(){
        return ResponseEntity.ok(userService.getUsers());
    }

    @PostMapping("/login")
    public ResponseEntity<LogUserDTO> login(@Valid @RequestBody  LoginDTO loginDTO ){
        LogUserDTO logedUserDTO = userService.login(loginDTO);
        return ResponseEntity.ok(logedUserDTO);
    }


}
