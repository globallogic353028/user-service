package com.nisum.userservice.controller;


import com.nisum.userservice.dto.LogUserDTO;
import com.nisum.userservice.dto.LoginDTO;
import com.nisum.userservice.dto.SignUpDTO;
import com.nisum.userservice.dto.UserDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;


@Api(value = "User Controller", description = "REST API for Users", tags = {"User Controller"})
public interface UserController {

     @ApiOperation(value = "Sign up User", response = SignUpDTO.class)
     @ApiResponses(value = {
             @ApiResponse(code = 200, message = "Successfully create user"),
             @ApiResponse(code = 409, message = "if User exist"),
     })
     @PostMapping()
     ResponseEntity<SignUpDTO> signUp(@Valid @RequestBody UserDTO userDTO);

     @ApiOperation(value = "Get all User", response = List.class)
     @ApiResponses(value = {
             @ApiResponse(code = 200, message = "For ok operation"),
             @ApiResponse(code = 500, message = "Internal server error"),
     })
     @GetMapping
     ResponseEntity<List<SignUpDTO>> getUsers();

     @ApiOperation(value = "Login User", response = LoginDTO.class)
     @ApiResponses(value = {
             @ApiResponse(code = 200, message = "Successfully Login user"),
             @ApiResponse(code = 400, message = "Bad request"),
             @ApiResponse(code = 401, message = "Unauthorized"),
             @ApiResponse(code = 403, message = "Invalid user and or password"),
             @ApiResponse(code = 498, message = "User Inactive"),
             @ApiResponse(code = 500, message = "Internal server error"),
     })
     @PostMapping()
     ResponseEntity<LogUserDTO> login(@Valid @RequestBody  LoginDTO loginDTO );
}
