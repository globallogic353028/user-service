package com.nisum.userservice.service;

import com.nisum.userservice.dto.SignUpDTO;
import com.nisum.userservice.dto.UserDTO;
import com.nisum.userservice.entity.User;
import com.nisum.userservice.exception.UserExistException;
import com.nisum.userservice.mapper.PhoneMapper;
import com.nisum.userservice.mapper.UserMapper;
import com.nisum.userservice.repository.UserRepository;
import com.nisum.userservice.security.JwtProvider;
import com.nisum.userservice.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mapstruct.factory.Mappers;
import org.mockito.*;


import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.context.annotation.ComponentScan;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;


import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = { ContextConfiguration.class })
@ComponentScan("com.nisum.*")
public class UserServiceTest {



    @Mock
    private UserRepository userRepository;


    @Mock
    private PasswordEncoder passwordEncoder;
    @InjectMocks
    private UserServiceImpl userService ;

    @Mock
    private JwtProvider jwtProvider;



    @Spy
    private PhoneMapper phoneMapper = Mappers.getMapper(PhoneMapper.class);

    @Spy
    private UserMapper configurationMapper=  Mappers.getMapper(UserMapper.class);

    @BeforeEach
    public void setup(){
        MockitoAnnotations.initMocks(this);
        UUID id = new UUID(1,2);
        User userMikeSave = User.builder().id(id)
                .email("miguelieno@gmail.com")
                .name("mike")
                .password("pepe")
                .token("un token")
                .created(LocalDateTime.now())
                .isActive(Boolean.TRUE)
                .build();
        when(userRepository.findUserByEmail("miguelieno@gmail.com")).thenReturn(Optional.of(userMikeSave));

        when(userRepository.save(ArgumentMatchers.any(User.class))).thenReturn(userMikeSave);


    }

    @Test()
    public void whenLoginEmailOfMike_thenReturnUserExist(){
        UserDTO userDTO = UserDTO.builder().email("miguelieno@gmail.com").name("mike").password("pepe").build();

        UserExistException thrown = Assertions
                .assertThrows(UserExistException.class, () -> {
                    userService.signUp(userDTO);
                }, "Exist User");

        Assertions.assertEquals(UserExistException.DEFAULT_MSG_USER_EXIST, thrown.getMessage());
    }

    @Test()
    public void whenLoginEmailOfMike_thenReturnSignUser(){
        UserDTO userDTO = UserDTO.builder().email("otro@gmail.com").name("mike").password("pepe").build();
        SignUpDTO signUpDTO = userService.signUp(userDTO);

        Assertions.assertNotNull(signUpDTO.getId());
        Assertions.assertNotNull(signUpDTO.getEmail());
        Assertions.assertTrue(signUpDTO.getActive());
        Assertions.assertNotNull(signUpDTO.getCreated());
        Assertions.assertNull(signUpDTO.getLastLogin());
        Assertions.assertTrue(userRepository.findUserByEmail(signUpDTO.getEmail()).isPresent());

    }


}
