plugins {
	java
	id("org.springframework.boot") version "2.7.13"
	id("io.spring.dependency-management") version "1.0.15.RELEASE"
}

group = "com.nisum"
version = "0.0.1-SNAPSHOT"

java {
	sourceCompatibility = JavaVersion.VERSION_1_8
}

configurations {
	compileOnly {
		extendsFrom(configurations.annotationProcessor.get())
	}
}

repositories {
	mavenCentral()
}

extra["springCloudVersion"] = "2021.0.8"

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.cloud:spring-cloud-starter-config")
	implementation("org.springframework.cloud:spring-cloud-starter-netflix-eureka-client")
	implementation("org.springframework.cloud:spring-cloud-starter-bootstrap")
	implementation ("org.springframework.boot", "spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-security")
	implementation ("io.jsonwebtoken", "jjwt", "0.9.1")
	implementation ("org.springframework.boot:spring-boot-starter-validation:2.4.0")

	implementation(group= "org.mapstruct", name= "mapstruct-jdk8", version= "1.2.0.Final")
	annotationProcessor( "org.mapstruct:mapstruct-processor:1.2.0.Final")
	compileOnly( "org.mapstruct:mapstruct-processor:1.2.0.Final")
	compileOnly("org.projectlombok:lombok")
	compileOnly("io.springfox", name="springfox-swagger2", version= "2.9.2")
	compileOnly("io.springfox", name="springfox-swagger-ui", version= "2.9.2")



	runtimeOnly("com.h2database:h2")
	annotationProcessor("org.projectlombok:lombok")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("junit:junit:4.13")

}

dependencyManagement {
	imports {
		mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
